﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
namespace Randomizer
{

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public string buildexe;
        private string getadd(string data)
        {
            List<string> mydata = new List<string>(data.Split(new char[] { '\t', ' ' }));
            List<string> truedata = new List<string>();
            foreach (string dataline in mydata)
            {
                if (dataline == null || dataline.Length <= 1)
                    continue;
                else
                    truedata.Add(dataline);
            }
            return truedata[0];
        }
        private void getmapdata(string inputdir,string outputdir)
        {
            string indir = inputdir;//"C:\\malware1";
            string outdir = outputdir;// "C:\\malware1\\mapdata";
            List<string> maps=new List<string>(Directory.GetFiles(inputdir+"\\Executables","*.map"));
            int tempcount=0;
            foreach(string file1 in maps)
            {

            //string file1 = indir + "\\Executables" + "\\sample0.map";
            string filename = file1.Substring(file1.LastIndexOf('\\'), file1.LastIndexOf('.') - file1.LastIndexOf('\\'));
            string outfile = outdir + filename + "Map.txt";
            File.Create(outfile).Dispose();
            List<string> lines = new List<string>(File.ReadAllLines(file1));
            int a = 0;
            do
            {
                a++;
            } while (!lines[a].Contains("Rva+Base"));
            lines.RemoveRange(0, a);
            a = 0;
            do
            {
                a++;
            } while (!lines[a].Contains("entry point at"));
            lines.RemoveRange(a, lines.Count - a);
            //File.AppendAllLines(outfile, lines);
            string file2 = indir + "\\" + filename + ".cfg";//indir + "\\Key.txt";//
            List<string> components = new List<string>();
            List<string> lines1 = new List<string>();
            lines1 = new List<string>(File.ReadAllLines(file2));
            foreach (string line in lines1)
            {
                if (line.Contains("\t"))
                {
                   //if(!components.Contains(line.Substring(line.LastIndexOf('\\') + 1, line.LastIndexOf('.') - line.LastIndexOf('\\') - 1)))
                       components.Add(line.Substring(line.LastIndexOf('\\') + 1, line.LastIndexOf('.') - line.LastIndexOf('\\') - 1));
                }
            }
            //   File.AppendAllLines(outfile,components);
            a = 0;
            List<string> imp = new List<string>();
            do
            {
                foreach (string compo in components)
                {
                    if (lines[a].Contains(compo))
                    {
                        imp.Add(lines[a] + "\t" + getadd(lines[a + 1]));
                        //imp.Add(getadd(lines[a + 1]));
                    }
                }
                a++;
            } while (a < lines.Count);
            imp.Sort();
            File.AppendAllLines(outfile, imp);
                tempcount++;
                progressBar1.Value = tempcount * 100 / maps.Count;
            }
            MessageBox.Show("Done");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult res=folderBrowserDialog1.ShowDialog();
            if (res == DialogResult.OK)
            {
                textBox1.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        //private void Callcmd(string cmd,bool direct)
        //{
        //    int ExitCode;
        //    ProcessStartInfo ProcessInfo;
        //    Process Process;
        //    if(direct)
        //        ProcessInfo = new ProcessStartInfo("cmd.exe", "/c "+cmd);
        //    else
        //        ProcessInfo = new ProcessStartInfo("cmd.exe", cmd);
        //    ProcessInfo.CreateNoWindow = true;
        //    ProcessInfo.UseShellExecute = false;
        //    ProcessInfo.WorkingDirectory = Application.StartupPath;
            
        //    ProcessInfo.RedirectStandardError = true;
        //    ProcessInfo.RedirectStandardOutput = true;

        //    Process = Process.Start(ProcessInfo);
        //    Process.WaitForExit();

        //    string output = Process.StandardOutput.ReadToEnd();
        //    string error = Process.StandardError.ReadToEnd();

        //    //MessageBox.Show("output=" + output);
        //    //MessageBox.Show("Error=" + error);
        //    ExitCode = Process.ExitCode;
        //    if(ExitCode!=0)
        //        MessageBox.Show("ExitCode: " + ExitCode.ToString(), "CMD");
        //    Process.Close();
        //}
        
        private void CallProg(string loc, string arg)
        {
            int ExitCode;
            ProcessStartInfo ProcessInfo;
            Process Process;
            
                ProcessInfo = new ProcessStartInfo(loc, arg);
            //ProcessInfo.CreateNoWindow = false;
            ProcessInfo.UseShellExecute = false;
            ProcessInfo.WorkingDirectory = Application.StartupPath;

            ProcessInfo.RedirectStandardError = true;
            ProcessInfo.RedirectStandardOutput = true;
            
            Process = Process.Start(ProcessInfo);
         
            Process.WaitForExit();

            string output = Process.StandardOutput.ReadToEnd();
            string error = Process.StandardError.ReadToEnd();

            //MessageBox.Show("output=" + output);
            //MessageBox.Show("Error=" + error);
            ExitCode = Process.ExitCode;
            if(ExitCode!=0)
                MessageBox.Show("ExitCode: " + ExitCode.ToString(), loc.Substring(loc.LastIndexOf('\\')+1));
            Process.Close();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult res=folderBrowserDialog1.ShowDialog();
            if (res == DialogResult.OK)
            {
                textBox2.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = textBox1.Text;
            
            List<String> inputdirs;
            if (System.IO.Directory.Exists(textBox1.Text) && System.IO.Directory.Exists(textBox2.Text))
            {
                string[] dirs = System.IO.Directory.GetDirectories(textBox1.Text);
                inputdirs = new List<String>(dirs);
            }
            else
            {
                MessageBox.Show("Error Invalid Source and/or Output Path(s)");
                return;
            }
 
            int min = Convert.ToInt32(textBox3.Text);
            int max = Convert.ToInt32(textBox4.Text);
            int count = Convert.ToInt32(textBox5.Text);
            if (max > inputdirs.Count)
            {
                MessageBox.Show("Max/Sample exceeds Total input dirs!...Resetting max");
                max = inputdirs.Count;
                textBox4.Text = max.ToString();
            }
            if (min < 0)
            {
                MessageBox.Show("Invalid Value in min..resetting to 0");
            }
            int i = 0;

            Random rnd = new Random();
            Random index = new Random();
            Random fileindex = new Random();
            progressBar1.Value = 0;
            for (i = 0; i < count; i++)
            {
               
                //one sample per iteration
                List<String> inputfiles=new List<string>();
                int j = rnd.Next(min, max + 1); //no of input dirs to use
                
                List<int> indexes=new List<int>(j); //directory index
                
                int k = 0;
                do          //select the directories
                {
                    int p = index.Next(inputdirs.Count);
                    
                    if (!indexes.Contains(p) || checkBox1.Checked )
                    {
                        indexes.Add(p);
                        List<string> dirfiles=new List<string>(System.IO.Directory.GetFiles(inputdirs[p]));
                        inputfiles.Add(dirfiles[fileindex.Next(dirfiles.Count())]);
                        k++;
                    }

                } while (k < j);
                
 
                //create new output file
                string keypath = System.IO.Path.Combine(textBox2.Text, "sample" + Convert.ToString(i) + ".cfg");
                System.IO.File.Create(keypath).Dispose();
                string samplename = "sample" + Convert.ToString(i) + ".cpp";
                String[] temp1 = new string[]
                {
                    samplename + " : " + Convert.ToString(j),
                    " "
                };
                System.IO.File.AppendAllLines(keypath, temp1);
                string pathstring = System.IO.Path.Combine(textBox2.Text, samplename);
                System.IO.File.Create(pathstring).Dispose();
                string[] main2 = new string[j * 2];
                k = 0;
                if (j > 0)
                {
                    foreach (string inputfile in inputfiles)//(int a in indexes)
                    {
                        //append file at index a in inputfiles to output file
                        temp1 = new string[]
                        {
                            "\t" + inputfile,//"\t" + inputfiles[a],
                            " "
                        };
                        System.IO.File.AppendAllLines(keypath, temp1);
                        //string inputfile = System.IO.Path.Combine(textBox1.Text, inputfiles[a]);
                        System.IO.File.AppendAllText(pathstring, System.IO.File.ReadAllText(inputfile));
                        temp1 = new string[]
                        {
                            " ",
                            " "
                        };
                        System.IO.File.AppendAllLines(pathstring, temp1);
                        //MessageBox.Show((inputfiles[a]).LastIndexOf('\\').ToString());
                        int t1 = inputfile.LastIndexOf('\\');
                        int t2 = inputfile.IndexOf('.');
                        main2[k] = inputfile.Substring(t1 + 1, t2 - t1 - 1) + "(argc,argv);";
                        //int t1 = (inputfiles[a]).LastIndexOf('\\');
                        //int t2 = (inputfiles[a]).IndexOf('.');
                        //main2[k] = (inputfiles[a]).Substring(t1 + 1, t2 - t1 - 1) + "(argc,argv);";
                        k++;
                        main2[k] = " ";
                        k++;
                    }
                }
                //write main fn
                string[] main1;
                if (j == 0)
                {
                    main1 = new string[]
                    {
                        "#include \"stdafx.h\"",
                        "int _tmain(int argc, _TCHAR* argv[])",
                        "{"
                    };
                }
                else
                {
                    main1 = new string[]
                    {
                        "int _tmain(int argc, _TCHAR* argv[])",
                        "{"
                    };
                }
                string[] main3 = new string[]
                {
                    " ",
                    "return 0;",
                    "}"

                };
                System.IO.File.AppendAllLines(pathstring, main1);
                System.IO.File.AppendAllLines(pathstring, main2);
                System.IO.File.AppendAllLines(pathstring, main3);

                progressBar1.Value = i * 100 / count;
            }

            progressBar1.Value = 100;
            MessageBox.Show("done");
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (System.IO.Directory.Exists(textBox6.Text))
            {
                //string buildexe;
                if (radioButton1.Checked == true)
                {
                    buildexe = "VCExpress";
                    textBox6.Text = "C:\\Program Files\\Microsoft Visual Studio 9.0\\Common7\\IDE";
                    textBox7.Text = "C:\\Documents and Settings\\Vivek\\My Documents\\Visual Studio 2008\\Projects\\Template";
                }
                else
                {
                    buildexe = "DevEnv";
                    textBox6.Text = "C:\\Program Files\\Microsoft Visual Studio 10.0\\Common7\\IDE";
                    textBox7.Text = "C:\\Documents and Settings\\Vivek\\My Documents\\Visual Studio 2010\\Projects\\Template";
                }
                if (System.IO.File.Exists(textBox6.Text + "\\"+buildexe+".exe"))
                {
                    progressBar1.Value = 0;
                    System.IO.Directory.CreateDirectory(textBox2.Text + "\\Executables");

                    List<string> files = new List<string>(System.IO.Directory.GetFiles(textBox2.Text, "*.cpp"));
                    int tempval=0;
                    foreach (string file in files)
                    {
                        //copy file to template.cpp
                        //Callcmd("cp \""+file +"\"" + " \""+textBox7.Text+"\\Template\\Template.cpp\"",true);
                        
                        System.IO.File.Copy(file , textBox7.Text+"\\Template\\Template.cpp",true);
                        //clean and build template project
                        int t1 = file.LastIndexOf('\\');
                        int t2 = file.IndexOf('.');
                        string logpath = System.IO.Path.Combine(textBox2.Text, "Executables\\"+file.Substring(t1 + 1, t2 - t1 - 1) +".log");
                        System.IO.File.Create(logpath).Dispose();
                        string[] tempstr = new string[] 
                        { 
                            "build log for : "+ file.Substring(t1 + 1, t2 - t1 - 1) + ".exe\"", 
                            " " 
                        };
                        System.IO.File.AppendAllLines(logpath, tempstr);
                        string loc = "\""+textBox6.Text+"\\"+buildexe+".exe\"";//"\"" + textBox6.Text + "\\MSBuild.exe\"";
                        string arg = "\"" + textBox7.Text + "\\Template.sln\"" + "/rebuild Release /project " + "\"" + textBox7.Text + "\\Template\\Template.";
                        if(radioButton1.Checked)
                            arg=arg+"vcproj\" /projectconfig Release /out \""+ logpath + "\"";
                        else
                            arg=arg+"vcxproj\" /projectconfig Release /out \"" + logpath + "\"";
                        CallProg(loc, arg);
                        //MessageBox.Show("loc="+loc +" arg="+arg);
                       
                        //copy exe to output directory
                        
                        //Callcmd("mv \"" + textBox7.Text + "\\Release\\Template.exe\"" + " \"" + textBox2.Text + "\\Executables\\" + file.Substring(t1 + 1, t2 - t1 - 1) + ".exe\"",true );
                       //Callcmd("mv \"" + textBox7.Text + "\\Release\\Template.pdb\"" + " \"" + textBox2.Text + "\\Executables\\" + file.Substring(t1 + 1, t2 - t1 - 1) + ".pdb\"", true);
                        //Callcmd("mv \"" + textBox7.Text + "\\Template\\Template.map\"" + " \"" + textBox2.Text + "\\Executables\\" + file.Substring(t1 + 1, t2 - t1 - 1) + ".map\"", true);
                        //Callcmd("mv \"" + textBox7.Text + "\\Template\\Release\\Template.asm\"" + " \"" + textBox2.Text + "\\Executables\\" + file.Substring(t1 + 1, t2 - t1 - 1) + ".asm\"", true);
                        
                        System.IO.File.Move(textBox7.Text + "\\Release\\Template.exe" , textBox2.Text + "\\Executables\\" + file.Substring(t1 + 1, t2 - t1 - 1) + ".exe");
                        System.IO.File.Move(textBox7.Text + "\\Release\\Template.pdb" , textBox2.Text + "\\Executables\\" + file.Substring(t1 + 1, t2 - t1 - 1) + ".pdb");
                        System.IO.File.Move(textBox7.Text + "\\Template\\Template.map" , textBox2.Text + "\\Executables\\" + file.Substring(t1 + 1, t2 - t1 - 1) + ".map");
                        System.IO.File.Move(textBox7.Text + "\\Template\\Release\\Template.asm", textBox2.Text + "\\Executables\\" + file.Substring(t1 + 1, t2 - t1 - 1) + ".asm");
                        //display progress........
                        tempval++;
                        progressBar1.Value = (tempval * 100) / files.Count;
                    }
                    progressBar1.Value = 100;
                    MessageBox.Show("Done");
                }
                else
                {
                    MessageBox.Show("Cannot find :- "+buildexe+".exe");
                }
            }
            else
                MessageBox.Show("Error! Visual Studio Common7/IDE directory does not exist");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            DialogResult res = folderBrowserDialog1.ShowDialog();
            if (res == DialogResult.OK)
            {
                textBox6.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            DialogResult res = folderBrowserDialog1.ShowDialog();
            if (res == DialogResult.OK)
            {
                textBox7.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            getmapdata(textBox2.Text,textBox2.Text+"\\Executables");
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked == true)
            {
                buildexe = "VCExpress";
                textBox6.Text = "C:\\Program Files\\Microsoft Visual Studio 9.0\\Common7\\IDE";
                textBox7.Text = "C:\\Documents and Settings\\Vivek\\My Documents\\Visual Studio 2008\\Projects\\Template";
            }
            else
            {
                buildexe = "DevEnv";
                textBox6.Text = "C:\\Program Files\\Microsoft Visual Studio 10.0\\Common7\\IDE";
                textBox7.Text = "C:\\Documents and Settings\\Vivek\\My Documents\\Visual Studio 2010\\Projects\\Template";
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked == true)
            {
                buildexe = "VCExpress";
                textBox6.Text = "C:\\Program Files\\Microsoft Visual Studio 9.0\\Common7\\IDE";
                textBox7.Text = "C:\\Documents and Settings\\Vivek\\My Documents\\Visual Studio 2008\\Projects\\Template";
            }
            else
            {
                buildexe = "DevEnv";
                textBox6.Text = "C:\\Program Files\\Microsoft Visual Studio 10.0\\Common7\\IDE";
                textBox7.Text = "C:\\Documents and Settings\\Vivek\\My Documents\\Visual Studio 2010\\Projects\\Template";
            }
        }
    }
}
